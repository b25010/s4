package com.zuitt.phonebook;

public class Contact {
    // properties/variables
    private String name;
    private String contactNumber;
    private String address;

    // constructor
    public Contact(){}

    public Contact(String name, String contactNumber, String address){
        this.name = name;
        this.contactNumber = contactNumber;
        this.address = address;
    }

    // getter and setter

    public String getName(){
        return this.name;
    }
    public String getContactNumber(){
        return this.contactNumber;
    }
    public String getAddress(){
        return this.address;
    }


    public void setName(String name){
        this.name = name;
    }
    public void setContactNumber(String contactNumber){
        this.contactNumber = contactNumber;
    }
    public void setAddress(String address){
        this.address = address;
    }
}
