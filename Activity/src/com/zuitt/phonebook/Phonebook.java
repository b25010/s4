package com.zuitt.phonebook;

import java.util.ArrayList;
import java.util.List;

import static java.lang.System.out;

public class Phonebook {

    private List<Contact> contactList = new ArrayList<>();

    public void  setContact2(Contact contact){
        contactList.add(contact);
    }

    public void getContact2List(){
        Boolean dummy = true;

        for(Contact contact : contactList){
            String contactNumber =  contact.getContactNumber();
            String contactAddress =  contact.getAddress();

            if(contactNumber == null || contactNumber == ""){ contactNumber = "un-registered"; }
            if(contactAddress == null || contactAddress == ""){ contactAddress = "un-registered"; }

            out.println(contact.getName() + '\t' + contactNumber + "\t\t" + contactAddress);
            dummy = false;
        }

        if(dummy){
            out.println("Empty Phonebook!");
        }
    }

//    public void getSpecificContact2(String name){
//        for(Contact contact: contactList){
//            String contactNumber =  contact.getContactNumber();
//            String contactAddress =  contact.getAddress();
//
//            if (contactNumber == " "){ contactNumber = "<No Contact number>";
//            }else{ contactNumber = contact.getContactNumber(); }
//
//            if (contactAddress == " "){ contactAddress = "<No Address>";
//            }else{ contactAddress = contact.getAddress(); }
//
//            if(contact.getName().equals(name)){
//                out.println(contact.getName() + " has the following contact details: \n");
//                out.println("Contact number: " + contactNumber + "\tAddress: " + contactAddress);
//
//            }
//        }
//    }


}
