package com.zuitt.example;

import static java.lang.System.out;

public class Child extends Parent{

    public void speak(){
        // we have the original method from the parent class.
        //super.speak();
        out.println("I am the child!");
    }
}
