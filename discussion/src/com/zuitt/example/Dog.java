package com.zuitt.example;
import static java.lang.System.out;

// Child class of Animal
    // "extends" keyword is used to inherit the properties and methods of the parent class.
public class Dog extends Animal{

    // properties
    private String breed;

    // Constructor
    public Dog(){
        // super() is used to have direct access with the original constructor(parent class)
        super();
        this.breed = "American Bulldog";
    }

    public Dog(String name, String color, String breed){
        super(name, color);
        this.breed = breed;
    }

    // setter and getter

    public String getBreed(){
        return this.breed;
    }

    public void setBreed(String breed){
        this.breed = breed;
    }

    // method
    public void speak(){
        out.println("Woof! Woof!");
    }

    public void  call(){
        super.call();
        out.println("Hi my name is " + this.name + ", I am a Dog.");
    }
}
