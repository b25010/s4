package com.zuitt.example;

public interface Greetings {
    void morningGreet();

    void holidayGreet();
}
