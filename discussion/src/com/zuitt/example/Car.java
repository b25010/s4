package com.zuitt.example;
import static java.lang.System.out;

public class Car {

    // there aer used to restrict the scope of a class, constructor, variable, method.

    // four types of Access Modifier:


    // Class creating
    // Four parts of class creation

    // 1. Properties - characteristics of an object;
        private  String name;
        private  String brand;
        private int yearOfMake;

        // Makes additional component of a car
        private Driver driver;

    // 2. Constructor - used to create/instantiate an object.
        // a. *Empty constructor - creates object that doesn't have any argument/parameters. Also referred as default constructor.
            public Car(){
                // this is to set default upon instantiation.
                this.yearOfMake = 2000;

                // whenever a new car is created, it will have a (default) driver named Alejandro
                this.driver = new Driver("Alejandro");
            };

        // b. *Parameterized constructor - creates an object with arguments/properties.
            public Car(String name, String brand, int yearOfMake){
                this.name = name;
                this.brand = brand;
                this.yearOfMake = yearOfMake;
                this.driver = new Driver("Alejandro");
            };

    // 3. Getters and Setters - get and set the values of each property of an object.
        // *Getters - retrieves the value of instantiated object.
            public String getName(){ return this.name; }

            public String getBrand(){ return this.brand; }

            public int getYearOfMake() { return this.yearOfMake; }

            public String getDriverName(){
                return this.driver.getName();
            }

        // *Setters - used to change the default value of an instantiated object.
            public void setName(String name){ this.name = name; }

            public void setBrand(String brand){ this.brand = brand; }

            public void setYearOfMake(int yearOfMake){
                // can also be modified to add validation
                if(yearOfMake <= 2022){
                    this.yearOfMake = yearOfMake;
                }
            }

            public void setDriver(String driver){
                // this will invoke the setName() method of the Drivers class.
                this.driver.setName(driver);
            }

    // 4. Methods - functions that an object can perform (actions).
            public void drive(){
                out.println("This car is running. Vroooom! Vrooom!");
            }
}
